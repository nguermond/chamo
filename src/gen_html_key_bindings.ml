
let p b = Printf.bprintf b

let print_ks b l =
  List.iter (fun k -> p b "<keys>%s</keys> " (Configwin.key_to_string k)) l

let gen_table =
  let f b (ks, command) =
    p b "<tr><td><keys>";
    List.iter (fun k -> p b "%s " (Configwin.key_to_string k)) ks ;
    p b "</keys></td><td><com>%s</com></td></tr>\n" command;
  in
  fun b ~title ~id l ->
    p b "<table id=%S>\n" id;
    p b "<tr><th colspan=\"2\" class=\"title\">%s</th></tr>\n" title;
    List.iter (f b) l;
    p b "</table>"

let gen_tables b l =
  p b "<div class=\"bindings\">\n";
  List.iter
    (fun (title, id, bindings) ->
       gen_table b ~title ~id bindings;
    ) l;
  p b "</div>\n"

let gen_groups b l =
  p b "<div class=\"binding-group\">\n";
  List.iter (gen_tables b) l ;
  p b "</div>\n"

let groups =
  [
    [ "Window", "gui",
      Ocf.get Ed_gui_rc.window_key_bindings ;

      "Minibuffer", "minibuffer",
      Ocf.get Ed_minibuffer_rc.key_bindings ;

      "Multiclip", "multiclip",
      Ocf.get Ed_multiclip_rc.key_bindings ;
    ] ;

    [ "Sourceview", "sourceview",
      Ocf.get Ed_sourceview_rc.key_bindings ;
    ] ;

    [
        (*"Odoc", "odoc",
           Ocf.get Ed_odoc_rc.key_bindings ;*)

        "OCaml mode", "ocaml",
        Ocf.get Ed_mode_ocaml_rc.key_bindings ;

        "Changelog mode", "changelog",
        Ocf.get Ed_mode_changelog_rc.key_bindings ;

        "Makefile mode", "makefile",
        Ocf.get Ed_mode_makefile_rc.key_bindings ;
      ]
  ]

let b = Buffer.create 256
let () = gen_groups b groups
let () = print_endline (Buffer.contents b)

  