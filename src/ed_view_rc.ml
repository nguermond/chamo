(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let rc_file = Ed_config.rc_file "views"

let group = ref Ocf.group
let add_to_group path o = group := Ocf.add !group path o
let group () = !group

let default_filename_view_patterns =
  [
    ".*\\.odoc$", "odoc" ;
    ".*\\.tdl$",  "tdl" ;
    ".*\\.todo$", "tdl" ;
    ".*\\.mclip$", "multiclip";
  ]

let filename_view_patterns =
  let o = Ocf.list Ocf.Wrapper.(pair string string)
    ~doc:("Associations between regular expressions on filename and the "^
     "view to use to open the file")
      default_filename_view_patterns
  in
  add_to_group ["view_from_filename_patterns"] o;
  o
  
let default_default_view = "sourceview"
let default_view = 
  let o = Ocf.string 
    ~doc:"Default view used to when opening files when no pattern on filename matched."
      default_default_view
  in
  add_to_group ["default_view"] o;
  o

let read () = Ocf.from_file (group()) rc_file
let write () = Ocf.to_file (group()) rc_file
