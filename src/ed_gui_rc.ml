(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open GdkKeysyms

let rc_file = Ed_config.rc_file "gui"

let group = ref Ocf.group
let add_to_group path o = group := Ocf.add !group path o
let add_group_to_group path g = group := Ocf.add_group !group path g
let group () = !group

let default_minibuffer_history_size = 50

let default_abort_binding = ([`CONTROL], _g)

let default_window_key_bindings  = [
  [[`CONTROL], _x; [`CONTROL], _c], "close_active_window" ;
  [[`CONTROL], _n], "new_window" ;
  [[`CONTROL], _x ; [`CONTROL], _s], "save_active_view" ;
  [[`CONTROL], _x ; [`CONTROL], _w], "save_active_view_as" ;
  [[`CONTROL], _x ; [`CONTROL], _f], "open_file" ;
  [[`CONTROL], _b], "cycle_view" ;
  [[`CONTROL], _Tab], "cycle_tab" ;
  [[`CONTROL;`SHIFT], _Tab], "rev_cycle_tab" ;
  [[`CONTROL], _x; [`CONTROL], _t], "new_tab" ;
  [[`CONTROL], _v], "split_vertically" ;
  [[`CONTROL], _x; [`SHIFT], _3], "split_vertically" ;
  [[`CONTROL], _h], "split_horizontally" ;
  [[`CONTROL], _x; [`SHIFT], _2], "split_horizontally" ;
  [[`CONTROL], _x ; [`SHIFT], _0], "destroy_active_view" ;
  [[`CONTROL], _x ; [`CONTROL], _x; [], _l], "print_key_bindings" ;
  [[`CONTROL], _x ; [], _v], "popup_pick_hidden_view" ;
  [[`CONTROL], _x ; [], _l], "pop_last_hidden_view" ;
  [[`CONTROL], GdkKeysyms._y], "paste" ;
  [[`CONTROL], GdkKeysyms._c], "copy" ;
  [[`CONTROL], GdkKeysyms._w], "cut" ;
  [[`MOD1], GdkKeysyms._x], "prompt_command" ;
]

let minibuffer_history_size =
  let o = Ocf.int ~doc: "The size of histories in minibuffer"
      default_minibuffer_history_size
  in
  add_to_group ["minibuffer_history_size"] o ;
  o

let abort_binding =
  let o = Configwin.key_option
    ~doc:"The key combination to use to reset the key stroke state"
    default_abort_binding
  in
  add_to_group ["abort_binding"] o;
  o

let window_key_bindings =
  let o = Ocf.list Ed_config.binding_wrappers
    ~doc: "Common key bindings for windows"
    default_window_key_bindings
  in
  add_to_group ["window_key_bindings"] o;
  o

let () = add_group_to_group ["log"] (Ed_log.group())

let read () = Ocf.from_file (group()) rc_file
let write () = Ocf.to_file (group()) rc_file

let _ = read () ; write ()

let trees_for_window additional =
  Okey.trees_of_list
    (List.map (fun (x,s) -> (x, fun () -> Ed_commands.eval_command s))
       (Ocf.get window_key_bindings @ additional))

let create_add_binding_commands
  (option:(Okey.keyhit_state * string) list Ocf.conf_option)
    name =
  let f_add = fun key_state command ->
    let l = (key_state, command) :: Ocf.get option in
    Ocf.set option l
  in
  let f_add_string = fun key_state_string ->
    let key_state =
      Ed_config.key_state_wrappers.Ocf.Wrapper.from_json
        (Yojson.Safe.from_string key_state_string)
    in
    f_add key_state
  in
  let com_name = Printf.sprintf "add_%s_key_binding" name in
  let f_com args =
    let len = Array.length args in
    if len < 2 then
      failwith (Printf.sprintf "Usage: %s <list of keys> <command>" com_name);
    f_add_string args.(0) args.(1)
  in
  let com =
    { Ed_commands.com_name = com_name;
      com_args = [| "list of keys" ; "command" |] ;
      com_more_args = None ;
      com_f = f_com ;
    }
  in
  Ed_commands.register com;
  (f_add, f_add_string)

let (add_window_key_binding, add_window_key_binding_string) =
  create_add_binding_commands window_key_bindings "window"


