(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let factory_name = "odoc"
let rc_file = Ed_config.rc_file factory_name

let group = ref Ocf.group
let add_to_group path o = group := Ocf.add !group path o
let group () = !group

let default_key_bindings  = [
]

let key_bindings =
  let o = Ocf.list Ed_config.binding_wrappers 
    ~doc: "Key bindings" default_key_bindings
  in
  add_to_group ["key_bindings"] o;
  o

let create_pix name def help =
  let o = Ocf.string ~doc:("Pixmap file used to represent "^help) def in
  add_to_group ["pixmap";name] o;
  o

let pix_file = create_pix "file"
    (Filename.concat Ed_installation.pixmap_dir "file_component.png")
    "files"
let pix_comp = create_pix "component"
    (Filename.concat Ed_installation.pixmap_dir "component.png")
    "classes and modules"
let pix_other = create_pix "other"
    (Filename.concat Ed_installation.pixmap_dir "run.png")
    "other stuff"
let pix_type = create_pix "type"
    (Filename.concat Ed_installation.pixmap_dir "type.png")
    "types"
let pix_fun = create_pix "function"
    (Filename.concat Ed_installation.pixmap_dir "fun.png")
    "functions and methods"
let pix_value = create_pix "value"
    (Filename.concat Ed_installation.pixmap_dir "value.png")
    "non functional values"


let read () = Ocf.from_file (group()) rc_file
let write () = Ocf.to_file (group()) rc_file
