(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_log.ml 758 2011-01-13 07:53:27Z zoggy $ *)

let log_src = Logs.Src.create Ed_messages.software

let group = ref Ocf.group
let add_to_group path o = group := Ocf.add !group path o; o
let group () = !group

let color s ?(doc=Printf.sprintf "color for %s messages" s) def =
  add_to_group ["colors"; s] (Ocf.string ~doc def)

let color_app = color "app" "white"
let color_error = color "error" "red"
let color_warning = color "warning" "orange"
let color_info = color "info" "green"
let color_debug = color "debug" "yellow"

let font = add_to_group ["font"]
  (Ocf.string ~doc:"font for messages" "fixed 10")

let level_wrapper =
  let to_j ?with_doc k = `String (Logs.level_to_string k) in
  let from_j ?def = function
  | `String s ->
      (match Logs.level_of_string s with
      | Ok l -> l
      | Error (`Msg _) ->
           failwith (Printf.sprintf "invalid log level %S" s)
      )
  | json ->
      let msg = Printf.sprintf
        "Invalid key %S" (Yojson.Safe.to_string json)
      in
      failwith msg
  in
  Ocf.Wrapper.make to_j from_j

let level = add_to_group ["level"]
  (Ocf.option ~doc:"log level"
    ~cb: (fun level -> Logs.Src.set_level log_src level)
    level_wrapper (Some Logs.Info))

let length = add_to_group ["length"]
  (Ocf.int ~doc:"number of characters kept in log" 100_000)

module LevMap =
  Map.Make(struct type t = Logs.level let compare = Stdlib.compare end)

class box () =
  let wscroll = GBin.scrolled_window
      ~hpolicy: `AUTOMATIC
      ~vpolicy: `AUTOMATIC () in
  let tag_table = GText.tag_table () in
  let buffer =  GSourceView3.source_buffer ~tag_table () in
  let () = Gtksv_utils.register_source_buffer buffer in
  let source_view = GSourceView3.source_view
      ~source_buffer: buffer
      ~cursor_visible: false
      ~wrap_mode: `CHAR
      ~editable: false ~packing: wscroll#add ()
  in
  let () = Gtksv_utils.register_source_view source_view in
  let () = Gtksv_utils.apply_sourceview_props source_view
    (Gtksv_utils.read_sourceview_props ())
  in
  let new_tag props =
    let t = GText.tag () in
    t#set_properties props;
    tag_table#add t#as_tag;
    t
  in
  let fixed_font = new_tag [`FONT (Ocf.get font)] in
  let levmap = List.fold_left
    (fun acc level ->
       let o = match level with
         | Logs.App -> color_app
         | Error -> color_error
         | Warning -> color_warning
         | Info -> color_info
         | Debug -> color_debug
       in
       let t = new_tag [`FOREGROUND (Ocf.get o)] in
       LevMap.add level t acc
    ) LevMap.empty [ App ; Error ; Warning ; Info ; Debug ]
  in

  object(self)
    method box = wscroll#coerce
    method print level s =
      let tag = LevMap.find level levmap in
      buffer#insert
        ~iter: buffer#end_iter
        ~tags: [fixed_font ; tag]
        (Glib.Convert.locale_to_utf8 s);
      let l = buffer#char_count in
      if l > Ocf.get length then
        (
         buffer#delete
           ~start: buffer#start_iter
           ~stop: (buffer#get_iter_at_char (max (Ocf.get length / 2) (String.length s)));
         buffer#insert ~iter: buffer#start_iter "...\n"
        )
  end

let log_window () =
  let window = GWindow.window ~kind: `TOPLEVEL ~show: false
      ~width: 500 ~height: 600 ()
      ~title: (Ed_messages.software ^ " log")
  in
  ignore (window#event#connect#delete (fun _ -> window#misc#hide (); true));
  let vbox = GPack.vbox ~packing: window#add () in
  let v = new box () in
  ignore(vbox#pack ~expand: true v#box);
  let wb_close = GButton.button
      ~label: Ed_messages.close
      ~packing: (vbox#pack ~expand: false)
      ()
  in
  ignore (wb_close#connect#clicked window#misc#hide);

  object
    method window = window
    method print = v#print
  end

let the_log_window = ref None

let get_log_window () =
  match !the_log_window with
  | Some w -> w
  | None ->
      let w = log_window () in
      the_log_window := Some w;
      w

let log level ?(to_utf8=false) s =
  let s =
    (* FIXME: convert from locale or from another charset ?
       The problem is that we can't use Ed_misc from here.
       This problem could be resolved by strongly typing utf8 strings.
       *)
    if to_utf8 then
      try Glib.Convert.locale_to_utf8 s
      with _ -> s
    else
      s
  in
  Logs.msg ~src:log_src level (fun p -> p "%s" s)

let app = log Logs.App
let err = log Logs.Error
let warn = log Logs.Warning
let info = log Logs.Info
let debug = log Logs.Debug

let show_log_window () =
  let w = get_log_window () in
  w#window#show ()

let hide_log_window () =
  let w = get_log_window () in
  w#window#misc#hide ()

let reporter () =
  let buf_fmt () =
    let b = Buffer.create 512 in
    Format.formatter_of_buffer b,
    fun () -> let m = Buffer.contents b in Buffer.reset b; m
  in
  let ppf, b_flush = buf_fmt () in
  (*let reporter = Logs.format_reporter ~app:ppf ~dst:ppf () in*)
  let report src level ~over k msgf =
    let k _ =
      let w = get_log_window () in
      w#print level (b_flush ());
      over (); k ()
    in
    msgf @@ fun ?header ?tags fmt ->
      Format.kfprintf k ppf ("[%s] @[" ^^ fmt ^^ "@]@.")
      (Logs.Src.name src)
  in
  { Logs.report = report }

let () =  Logs.set_reporter (reporter())

