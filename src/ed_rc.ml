(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(* $Id: cam_rc.ml 758 2011-01-13 07:53:27Z zoggy $ *)

let rc_dir = Ed_installation.rc_dir

(** {2 Core options} *)

let rc_core = Filename.concat rc_dir "core.ini"
let core_ini = ref Ocf.group
let add_to_core_ini path o = core_ini := Ocf.add !core_ini path o
let save_core () = Ocf.to_file !core_ini rc_core
let load_core () = Ocf.from_file !core_ini rc_core

(** {2 Gui options} *)

let rc_gui = Filename.concat rc_dir "gui.ini"
let gui_ini = ref Ocf.group
let add_to_gui_ini path o = gui_ini := Ocf.add !gui_ini path o
let save_gui () = Ocf.to_file !gui_ini rc_gui
let load_gui () = Ocf.from_file !gui_ini rc_gui

(** {2 Utils} *)

let add_binding map binding action =
  map#set ((Configwin.string_to_key binding, action) :: map#get)
