(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let mode_name = "ocaml"
let includes_global_var = mode_name^"_includes"

let rc_file = Ed_sourceview_rc.mode_rc_file mode_name
let local_rc_file = Ed_sourceview_rc.local_mode_rc_file mode_name

let group = ref Ocf.group
let add_to_group path o = group := Ocf.add !group path o
let group () = !group

let x = 1 - 1
let default_key_bindings  = [
    [[], GdkKeysyms._Tab], mode_name^"_indent_line" ;
    [[`CONTROL], GdkKeysyms._x ; [`CONTROL], GdkKeysyms._Tab], mode_name^"_indent_buffer";
    [[`CONTROL], GdkKeysyms._x ; [`CONTROL], GdkKeysyms._a], mode_name^"_switch_file";
    [[`CONTROL], GdkKeysyms._o ; [`CONTROL], GdkKeysyms._c], mode_name^"_build";
    [[`MOD1], GdkKeysyms._t], mode_name^"_display_type_annot";
    [[`CONTROL; `MOD1], GdkKeysyms._t], mode_name^"_copy_type_annot";
    [[`MOD1], GdkKeysyms._c], mode_name^"_display_call_annot";
    [[`MOD1], GdkKeysyms._i], mode_name^"_display_ident_annot";
    [[`MOD1], GdkKeysyms._j], mode_name^"_jump_to_local_def";
    [[`CONTROL;`MOD1], GdkKeysyms._c], mode_name^"_show_stack_calls";
    [[`CONTROL;`MOD1], GdkKeysyms._x], mode_name^"_expand_ext_idents";
]

let key_bindings = 
  let o = Ocf.list Ed_config.binding_wrappers
    ~doc:"Key bindings" default_key_bindings
  in 
  add_to_group ["key_bindings"] o;
  o

let stack_call_bgcolor =
  let o = Ocf.string ~doc:"Background color of non-tail calls" "red" in
  add_to_group ["stack_call_colors";"background"] o;
  o
  
let stack_call_fgcolor =
  let o = Ocf.string ~doc:"Foreground color of non-tail calls" "yellow" in
  add_to_group ["stack_call_colors";"foreground"] o;
  o

let read () = Ocf.from_file (group()) rc_file
let write () = Ocf.to_file (group()) rc_file

let (add_sourceview_mode_ocaml_key_binding,
   add_sourceview_mode_ocaml_key_binding_string) =
  Ed_sourceview_rc.create_add_sourceview_mode_binding_commands
    key_bindings mode_name;;

let local_group = ref Ocf.group
let add_to_local_group path o = local_group := Ocf.add !local_group path o
let local_group () = !local_group

let ocamlbuild_commands =
  let o = Ocf.list Ocf.Wrapper.(pair string string)
    ~doc:"ocamlbuild commands associated to edited files"
    []
  in
  add_to_local_group ["ocamlbuild_commands"] o;
  o

let local_read () = Ocf.from_file (local_group()) local_rc_file;;
let local_write () =
  match Ocf.get ocamlbuild_commands with
  [] -> (try Sys.remove local_rc_file with _ -> ())
  | _ -> Ocf.to_file (local_group()) local_rc_file
;;

 