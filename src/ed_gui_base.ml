(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

class main () =
  let toplevel = GWindow.window ~title:"Chamo" () in
  let () = toplevel#set_default_size ~width:600 ~height:500 in
  let vbox = GPack.vbox ~packing:toplevel#add () in
  let hboxmenus = GPack.hbox ~packing:(vbox#pack ~expand:false ~fill:true) () in
  let menubar = GMenu.menu_bar ~packing:(hboxmenus#pack ~expand:false ~fill:true) () in
  let item_file = GMenu.menu_item ~label:"File"
    ~packing:(menubar#insert ~pos:0) () in
  let menu_file = GMenu.menu ~packing:item_file#set_submenu () in
  let item_new_window = GMenu.menu_item ~label:"New window" ~packing:menu_file#add () in
  let item_open = GMenu.menu_item ~label:"Open" ~packing:menu_file#add () in
  let item_save = GMenu.menu_item ~label:"Save" ~packing:menu_file#add () in
  let item_save_as = GMenu.menu_item ~label:"Save as ..." ~packing:menu_file#add () in
  let item_reload = GMenu.menu_item ~label:"Reload" ~packing:menu_file#add () in
  let item_prefs = GMenu.menu_item ~label:"Preferences" ~packing:menu_file#add () in
  let item_log_window = GMenu.menu_item ~label:"Log window" ~packing:menu_file#add () in
  let item_close = GMenu.menu_item ~label:"Close" ~packing:menu_file#add () in
  let item_edit = GMenu.menu_item ~label:"Edit"
    ~packing:(menubar#insert ~pos:1) () in

  let item_view = GMenu.menu_item ~label:"View" ~packing:(menubar#insert ~pos:2) () in
  let menu_view = GMenu.menu ~packing:item_view#set_submenu () in
  let item_new_tab = GMenu.menu_item ~label:"New tab" ~packing:menu_view#add () in
  let item_split_horizontally = GMenu.menu_item ~label:"Split horizontally" ~packing:menu_view#add () in
  let item_split_vertically = GMenu.menu_item ~label:"Split vertically" ~packing:menu_view#add () in
  let item_destroy = GMenu.menu_item ~label:"Destroy active view" ~packing:menu_view#add () in
  let item_store_layout = GMenu.menu_item ~label:"Store layout" ~packing:menu_view#add () in
  let item_cycle_tab = GMenu.menu_item ~label:"Cycle tab" ~packing:menu_view#add () in
  let item_cycle_view = GMenu.menu_item ~label:"Cycle view" ~packing:menu_view#add () in

  let view_menubar = GMenu.menu_bar ~packing:(hboxmenus#pack ~expand:false ~fill:true) () in

  let help_menubar = GMenu.menu_bar ~packing:(hboxmenus#pack ~expand:true ~fill:true) () in
  let item_help = GMenu.menu_item ~label:"Help" ~packing:(help_menubar#insert ~pos:0) () in
  let menu_help = GMenu.menu ~packing:item_help#set_submenu () in
  let item_about = GMenu.menu_item ~label:"About" ~packing:menu_help#add () in

  let hbox_state = GPack.hbox ~packing:(vbox#pack ~expand:false ~fill:true) () in
  let wl_keystate = GEdit.entry ~editable:false ~width:80 ~has_frame:false
    ~packing:(hbox_state#pack ~expand:false ~fill: true) () in
  let () = wl_keystate#misc#set_sensitive false in
  object (self)
    val toplevel = toplevel
    method toplevel = toplevel
    method main = toplevel
    val vbox = vbox
    method vbox = vbox
    val hboxmenus = hboxmenus
    method hboxmenus = hboxmenus
    val menubar = menubar
    method menubar = menubar
    val item_file = item_file
    method item_file = item_file
    val menu_file = menu_file
    method menu_file = menu_file
    val item_new_window = item_new_window
    method item_new_window = item_new_window
    val item_open = item_open
    method item_open = item_open
    val item_save = item_save
    method item_save = item_save
    val item_save_as = item_save_as
    method item_save_as = item_save_as
    val item_reload = item_reload
    method item_reload = item_reload
    val item_prefs = item_prefs
    method item_prefs = item_prefs
    val item_log_window = item_log_window
    method item_log_window = item_log_window
    val item_close = item_close
    method item_close = item_close
    val item_edit = item_edit
    method item_edit = item_edit

    val item_new_tab = item_new_tab
    method item_new_tab = item_new_tab

    val item_split_horizontally = item_split_horizontally
    method item_split_horizontally = item_split_horizontally

    val item_split_vertically = item_split_vertically
    method item_split_vertically = item_split_vertically

    val item_destroy = item_destroy
    method item_destroy = item_destroy

    val item_store_layout = item_store_layout
    method item_store_layout = item_store_layout

    val item_cycle_tab = item_cycle_tab
    method item_cycle_tab = item_cycle_tab

    val item_cycle_view = item_cycle_view
    method item_cycle_view = item_cycle_view

    val view_menubar = view_menubar
    method view_menubar = view_menubar

    val item_about = item_about
    method item_about = item_about


    val hbox_state = hbox_state
    method hbox_state = hbox_state
    val wl_keystate = wl_keystate
    method wl_keystate = wl_keystate

(*
    val item_save =
      new GMenu.image_menu_item (GtkMenu.ImageMenuItem.cast
        (Glade.get_widget_msg ~name:"item_save" ~info:"GtkImageMenuItem" xmldata))
    method item_save = item_save
    val item_save_as =
      new GMenu.image_menu_item (GtkMenu.ImageMenuItem.cast
        (Glade.get_widget_msg ~name:"item_save_as" ~info:"GtkImageMenuItem" xmldata))
    method item_save_as = item_save_as
    val item_reload =
      new GMenu.image_menu_item (GtkMenu.ImageMenuItem.cast
        (Glade.get_widget_msg ~name:"item_reload" ~info:"GtkImageMenuItem" xmldata))
    method item_reload = item_reload
    val item_log_window =
      new GMenu.image_menu_item (GtkMenu.ImageMenuItem.cast
        (Glade.get_widget_msg ~name:"item_log_window" ~info:"GtkImageMenuItem" xmldata))
    method item_log_window = item_log_window
    val menuEdit =
      new GMenu.menu_item (GtkMenu.MenuItem.cast
        (Glade.get_widget_msg ~name:"menuEdit" ~info:"GtkMenuItem" xmldata))
    method menuEdit = menuEdit
    val menuitem6_menu =
      new GMenu.menu (GtkMenu.Menu.cast
        (Glade.get_widget_msg ~name:"menuitem6_menu" ~info:"GtkMenu" xmldata))
    method menuitem6_menu = menuitem6_menu
    val viewmenubar =
      new GMenu.menu_shell (GtkMenu.MenuBar.cast
        (Glade.get_widget_msg ~name:"viewmenubar" ~info:"GtkMenuBar" xmldata))
    method viewmenubar = viewmenubar
    val menuitem11_menu =
      new GMenu.menu (GtkMenu.Menu.cast
        (Glade.get_widget_msg ~name:"menuitem11_menu" ~info:"GtkMenu" xmldata))
    method menuitem11_menu = menuitem11_menu
    val hbox_state =
      new GPack.box (GtkPack.Box.cast
        (Glade.get_widget_msg ~name:"hbox_state" ~info:"GtkHBox" xmldata))
    method hbox_state = hbox_state
    val wl_keystate =
      new GEdit.entry (GtkEdit.Entry.cast
        (Glade.get_widget_msg ~name:"wl_keystate" ~info:"GtkEntry" xmldata))
    method wl_keystate = wl_keystate
*)
    method reparent parent =
      vbox#misc#reparent parent;
      toplevel#destroy ()
  end
class outputs () =
  let toplevel = GWindow.window ~title:"Chamo outputs" () in
  let () = toplevel#set_default_size ~width:700 ~height:150 in
  let notebook = GPack.notebook ~packing:toplevel#add () in
  object (self)
    val toplevel = toplevel
    method toplevel = toplevel
    val notebook = notebook
    method notebook = notebook
    method reparent parent =
      notebook#misc#reparent parent ;
      toplevel#destroy ()
  end
class outputs_note_tab () =
  let toplevel = GWindow.window() in
  let hbox = GPack.hbox ~packing:toplevel#add () in
  let wlabel = GMisc.label ~packing:(hbox#pack ~expand:false ~fill:true) () in
  let wb_close = GButton.button
    ~packing:(hbox#pack ~expand:false ~fill:true) ()
  in
  let wb_image = GMisc.image ~stock:`CLOSE () in
  let () = wb_close#set_image wb_image#coerce in
  object (self)
    val hbox = hbox
    method hbox = hbox
    val wlabel = wlabel
    method wlabel = wlabel
    val wb_close = wb_close
    method wb_close = wb_close
    method reparent parent =
      hbox#misc#reparent parent ;
      toplevel#destroy ()
  end
