
let re = Str.regexp "[ \n\t]+"
let f stog env ?loc atts subs =
  let open Xtmpl_rewrite in
  let nbsp = node ("","nbsp") [] in
  let xmls =
    match merge_cdata_list subs with
    | [D { Xtmpl_xml.text } ] ->
        let l = Str.split_delim re text in
        let rec iter acc = function
        | [] -> List.rev acc
        | "" :: q -> iter acc q
        | s :: q ->
            let atts = atts_one ("","class") [cdata "keys"] in
            let t = node ("","span") ~atts [cdata s] in
            let acc = match acc with [] -> [t] | l -> t :: nbsp :: acc in
            iter acc q
        in
        iter [] l
    | _ -> [%xtmpl.string {| <error_>&lt;keys&gt; should contains cdata and only cdata</error_> |} ] ()
  in
  (stog, xmls)

let () = Stog_plug.register_html_base_rule ("","keys") f
